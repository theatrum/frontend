################################################################################
FROM node:20-alpine As build

ARG NODE_ENV
ARG GID=1000
ARG UID=1000
ARG USER=theatrum

RUN apk add shadow && \
    deluser --remove-home node && \
    groupadd -f -g ${GID} ${USER} && \
    useradd -m -g ${USER} -u ${UID} ${USER}

WORKDIR /app
RUN chown -R ${USER}:${USER} .

USER ${USER}
COPY --chown=${USER}:${USER} . .

RUN if [ "${NODE_ENV}" = "production" ]; then \
  npm clean-install --include=dev && \
  npm run build && \
  npm prune --production && \
  npm cache clean --force;\
  fi

###############################################################################
FROM build as development

ENV NODE_ENV=development
RUN npm install

EXPOSE 8080

CMD [ "npm", "run", "dev"]

###############################################################################
FROM nginx:stable-alpine as production

ENV NODE_ENV=production

RUN mkdir /app

COPY --chown=nginx:nginx --from=build /app/dist /app
COPY docker/nginx/nginx.conf /etc/nginx/nginx.conf

COPY --chown=nginx:nginx docker/nginx/docker-entrypoint.sh /usr/local/bin/docker-entrypoint

## add permissions
RUN chown -R nginx:nginx /var/cache/nginx && \
        chown -R nginx:nginx /var/log/nginx && \
        chown -R nginx:nginx /etc/nginx/conf.d && \
        chown -R nginx:nginx /usr/local/bin/docker-entrypoint

RUN touch /var/run/nginx.pid && \
        chown -R nginx:nginx /var/run/nginx.pid

## switch to non-root user
USER nginx

EXPOSE 8080

ENTRYPOINT [ "docker-entrypoint" ]