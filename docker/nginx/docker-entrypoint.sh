#!/bin/sh

ROOT_DIR=/app

for file in $ROOT_DIR/assets/*.js;
do
  echo "Processing $file ...";

  sed -i 's|VITE_APP_API_SERVER_V1|'${VITE_APP_API_SERVER_V1}'|g' $file 

done

nginx -g "daemon off;"